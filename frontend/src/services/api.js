import axios from 'axios';

const api = axios.create({
    // baseURL: 'https://jean-omnistack-backend.herokuapp.com',
    baseURL: 'http://localhost:3003',
});

export default api;