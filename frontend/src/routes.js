import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Boxes from './pages/Boxes';
import Box from './pages/Box';

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route path='/' exact component={Boxes} />
            <Route path='/box/:id' component={Box} />
        </Switch>
    </BrowserRouter>
);

export default Routes;